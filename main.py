import gc
import json

# from transformers import AutoModelForCausalLM, AutoTokenizer
from ctransformers import AutoModelForCausalLM, AutoTokenizer

conversation_history = []

# Load the configuration file
with open("config.json", "r") as config_file:
    config = json.load(config_file)

load_models_on_startup: bool = config.get("load-models-on-startup", False)
automatic_cleanup: bool = config.get("automatic-cleanup", False)
gpu_layers: int = config.get("gpu-layers", 0)

if load_models_on_startup:
    zephyr_7B_beta = AutoModelForCausalLM.from_pretrained(
        "TheBloke/zephyr-7B-beta-GGUF",
        model_file="zephyr-7b-beta.Q4_K_M.gguf",
        model_type="mistral",
        gpu_layers=gpu_layers
    )
    zephyr_7B_beta_tokenizer = AutoTokenizer.from_pretrained("TheBloke/zephyr-7B-beta-GGUF")
    openhermes_mistral_7B = AutoModelForCausalLM.from_pretrained(
        "TheBloke/OpenHermes-2.5-Mistral-7B-GGUF",
        model_file="openhermes-2.5-mistral-7b.Q4_K_M.gguf",
        model_type="mistral",
        gpu_layers=gpu_layers
    )
    openhermes_mistral_7B_tokenizer = AutoTokenizer.from_pretrained("TheBloke/OpenHermes-2.5-Mistral-7B-GGUF")


def load_model_and_tokenizer(model_name: str = "Undefined", model_file: str = "Undefined", model_type: str = "Undefined", has_tokenizer: bool = False):
    # Adjust for your need and depending on how much GPU power you have in your Rig. 0 means no GPU accel at all.
    # Value of this is located inside the config.json file
    global gpu_layers

    if model_name == "Undefined":
        print("No model_name passed")
        return None, None

    if model_file == "Undefined":
        print("No model_file passed")
        return None, None

    if model_type == "Undefined":
        print("No model_type passed")
        return None, None

    if has_tokenizer:
        tokenizer = AutoTokenizer.from_pretrained(model_name)
    else:
        tokenizer = None

    model = AutoModelForCausalLM.from_pretrained(model_name, model_file=model_file, model_type=model_type, gpu_layers=gpu_layers)

    return tokenizer, model


def process_input(input_text: str = "None", model_name: str = "Undefined", model_file: str = "Undefined", model_type: str = "Undefined", has_tokenizer: bool = False):
    global automatic_cleanup

    response: str = "No response given, input was likely processed wrong."

    tokenizer, model = load_model_and_tokenizer(model_name, model_file, model_type, has_tokenizer=has_tokenizer)

    if tokenizer is None:
        response = model(input_text)
    else:
        inputs = tokenizer(input_text, return_tensors="pt")
        output = model.generate(**inputs)
        response = tokenizer.decode(output[0], skip_special_tokens=True)

    if automatic_cleanup:
        # Clean-up. This will essentially delete the model from your VRAM and RAM after it has answered. Good for low-end PCs
        # which feature minimal specs, but this increases answering times significantly, as each new question requires
        # the full model to be loaded again.
        del model
        del tokenizer
        gc.collect()  # Explicitly call garbage collector

    return response


def process_input_with_preloaded_models(input_text: str = "None", model_name: str = "Undefined"):
    response: str = "No response given, input was likely processed wrong."

    if model_name == "TheBloke/zephyr-7B-beta-GGUF":
        # response = zephyr_7B_beta(input_text)
        inputs = zephyr_7B_beta_tokenizer(input_text, return_tensors="pt")
        outputs = zephyr_7B_beta.generate(**inputs, max_new_tokens=20)
        response = zephyr_7B_beta_tokenizer.decode(outputs[0], skip_special_tokens=True)
    elif model_name == "TheBloke/OpenHermes-2.5-Mistral-7B-GGUF":
        # response = openhermes_mistral_7B(input_text)
        inputs = openhermes_mistral_7B_tokenizer(input_text, return_tensors="pt")
        outputs = openhermes_mistral_7B.generate(**inputs, max_new_tokens=20)
        response = openhermes_mistral_7B_tokenizer.decode(outputs[0], skip_special_tokens=True)

    return response


def gating_mechanism(input_text):
    first_word = input_text.split()[0].lower()
    model_name: str = ""
    model_file: str = ""
    model_type: str = ""

    if first_word == "zephyr":
        model_name = "TheBloke/zephyr-7B-beta-GGUF"
        model_file = "zephyr-7b-beta.Q4_K_M.gguf"
        model_type = "mistral"
    elif first_word == "mistral":
        model_name = "TheBloke/OpenHermes-2.5-Mistral-7B-GGUF"
        model_file = "openhermes-2.5-mistral-7b.Q4_K_M.gguf"
        model_type = "mistral"
    else:
        return "No specific model triggered."

    if load_models_on_startup:
        response = process_input_with_preloaded_models(input_text, model_name)
    else:
        response = process_input(input_text, model_name, model_file, model_type)
    conversation_history.append({"input": input_text, "response": response})

    return response


# Continuous CLI interaction
while True:
    input_text = input("Enter your query (type 'exit' to quit): ")

    if input_text.lower() in ['exit', 'quit']:
        print("Exiting the program.")
        break

    result = gating_mechanism(input_text)
    print(result)

# Print conversation history
# print("Conversation History:")
# for entry in conversation_history:
#     print(f"Input: {entry['input']}")
#     print(f"Response: {entry['response']}")
